CREATE TABLE PRODUCTS(
        id SERIAL PRIMARY KEY,
        name VARCHAR NOT NULL,
        description VARCHAR NOT NULL,
        baseprice DECIMAL NOT NULL,
        taxrate DECIMAL NOT NULL,
        status VARCHAR NOT NULL,
        inventoryquantity INT NOT NULL
    );