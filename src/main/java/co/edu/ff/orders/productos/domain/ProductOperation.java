package co.edu.ff.orders.productos.domain;

import co.edu.ff.orders.productos.entity.Product;

public interface ProductOperation {

    Product value();
    String failure();
    Boolean isValid();
}
