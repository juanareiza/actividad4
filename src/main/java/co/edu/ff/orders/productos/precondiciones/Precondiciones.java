package co.edu.ff.orders.productos.precondiciones;

import java.math.BigDecimal;

public class Precondiciones {

    public static  void nonNullLong(Long value){ if (value == null) throw new IllegalArgumentException(); }
    public static  void mayorLong(Long value){ if (value > 1) throw new IllegalArgumentException(); }
    public static void nonNullString(String value) { if ( value == null || value.equals("")) throw new IllegalArgumentException(); }
    public static void maxLengthString(String value) { if (value.length() > 100) throw new IllegalArgumentException(); }
    public static void maxLengthStringDes(String value) { if (value.length() > 280) throw new IllegalArgumentException(); }
    public static  void nonNullBig(BigDecimal value){ if (value == null) throw new IllegalArgumentException(); }
    public static  void mayorBig(BigDecimal value){ if (value.intValue() < 1) throw new IllegalArgumentException(); }
    public static  void mayorMenorBig(BigDecimal value){ if (value.intValue() < 0 || value.intValue() > 1) throw new IllegalArgumentException(); }
    public static void inferiorInteger(Integer value){ if(value<0) throw new IllegalArgumentException();}
    public static void nonNullInteger(Integer value){if(value == null) throw new IllegalArgumentException();}
}
