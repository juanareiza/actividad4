package co.edu.ff.orders.productos.entity;

import co.edu.ff.orders.productos.precondiciones.Precondiciones;
import co.edu.ff.orders.productos.serialization.BigSerialization;
import lombok.Value;

import java.math.BigDecimal;

@Value(staticConstructor = "of")
public class BasePrice implements BigSerialization {

    BigDecimal basePrice;

    private BasePrice(BigDecimal basePrice){
        Precondiciones.nonNullBig(basePrice);
        Precondiciones.mayorBig(basePrice);
        this.basePrice = basePrice;
    }

    @Override
    public BigDecimal valueOf() {
        return basePrice;
    }
}
