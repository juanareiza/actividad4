package co.edu.ff.orders.productos.entity;

import co.edu.ff.orders.productos.precondiciones.Precondiciones;
import lombok.Value;

import java.math.BigDecimal;

@Value(staticConstructor = "of")
public class ProductOperationRequest {
    String name;
    String description;
    BigDecimal basePrice;
    BigDecimal taxRate;
    String productStatus;
    Integer inventoryQuantity;

    private ProductOperationRequest(String name, String description, BigDecimal basePrice, BigDecimal taxRate, String productStatus, Integer inventoryQuantity){
        Precondiciones.nonNullBig(basePrice);
        Precondiciones.nonNullBig(taxRate);
        Precondiciones.nonNullString(name);
        Precondiciones.nonNullString(description);
        Precondiciones.nonNullString(productStatus);
        Precondiciones.nonNullInteger(inventoryQuantity);
        this.name = name;
        this.description = description;
        this.basePrice = basePrice;
        this.taxRate = taxRate;
        this.productStatus = productStatus;
        this.inventoryQuantity = inventoryQuantity;
    }
}
