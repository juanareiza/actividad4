package co.edu.ff.orders.productos.entity;

import co.edu.ff.orders.productos.precondiciones.Precondiciones;
import lombok.Value;

@Value(staticConstructor = "from")
public class Product {
    Long id;
    Name name;
    Description description;
    BasePrice basePrice;
    TaxRate taxRate;
    ProductStatus productStatus;
    InventoryQuantity inventoryQuantity;

    private Product(Long id, Name name, Description description, BasePrice basePrice, TaxRate taxRate, ProductStatus productStatus, InventoryQuantity inventoryQuantity){
        Precondiciones.nonNullLong(id);
        this.id = id;
        this.name = name;
        this.description = description;
        this.basePrice = basePrice;
        this.taxRate = taxRate;
        this.productStatus = productStatus;
        this.inventoryQuantity = inventoryQuantity;
    }
}
