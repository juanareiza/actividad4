package co.edu.ff.orders.productos.exceptions;

import co.edu.ff.orders.productos.entity.Product;
import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value(staticConstructor = "of")
public class ProductDoesNotExists extends ProductException {

    Long product;

    private ProductDoesNotExists (Long id) {
        super(String.format("Product %s does not exists", id));
        this.product = id;
    }

}
