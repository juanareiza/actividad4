package co.edu.ff.orders.productos.entity;

public enum ProductStatus {
    BORRADOR,
    PUBLICADO
}
