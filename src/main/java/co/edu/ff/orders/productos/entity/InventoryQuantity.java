package co.edu.ff.orders.productos.entity;

import co.edu.ff.orders.productos.precondiciones.Precondiciones;
import co.edu.ff.orders.productos.serialization.IntegerSerialization;
import lombok.Value;

@Value(staticConstructor = "of")
public class InventoryQuantity implements IntegerSerialization {
    Integer inventoryQuantity;

    private InventoryQuantity(Integer inventoryQuantity){
        Precondiciones.nonNullInteger(inventoryQuantity);
        Precondiciones.inferiorInteger(inventoryQuantity);
        this.inventoryQuantity = inventoryQuantity;
    }

    @Override
    public Integer valueOf() {
        return inventoryQuantity;
    }
}
