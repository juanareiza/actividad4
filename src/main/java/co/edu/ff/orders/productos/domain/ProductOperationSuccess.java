package co.edu.ff.orders.productos.domain;

import co.edu.ff.orders.productos.entity.Product;
import lombok.Value;

@Value(staticConstructor = "of")
public class ProductOperationSuccess implements ProductOperation {
    Product product;

    @Override
    public Product value() {

        return product;
    }

    @Override
    public String failure() {
        return null;
    }

    @Override
    public Boolean isValid() {

        if (product != null){
            return true;
        }
        return false;
    }
}
