package co.edu.ff.orders.productos.repository;

import co.edu.ff.orders.productos.domain.ProductOperation;
import co.edu.ff.orders.productos.domain.ProductOperationFailure;
import co.edu.ff.orders.productos.domain.ProductOperationSuccess;
import co.edu.ff.orders.productos.entity.*;
import co.edu.ff.orders.productos.exceptions.ProductDoesNotExists;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@AllArgsConstructor
public class ProductRepository {
    JdbcTemplate jdbcTemplate;

    public ProductOperation insertOne(ProductOperationRequest productOperationRequest){
        try {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
                    .withTableName("products")
                    .usingGeneratedKeyColumns("id");

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("name", productOperationRequest.getName());
            parameters.put("description", productOperationRequest.getDescription());
            parameters.put("baseprice", productOperationRequest.getBasePrice());
            parameters.put("taxrate", productOperationRequest.getTaxRate());
            parameters.put("status", productOperationRequest.getProductStatus());
            parameters.put("inventoryquantity", productOperationRequest.getInventoryQuantity());
                Number key = simpleJdbcInsert.executeAndReturnKey(parameters);
            Product product = Product.from(key.longValue(), Name.of(productOperationRequest.getName()), Description.of(productOperationRequest.getDescription()),BasePrice.of(productOperationRequest.getBasePrice()),
                    TaxRate.of(productOperationRequest.getTaxRate()),ProductStatus.valueOf(productOperationRequest.getProductStatus()),InventoryQuantity.of(productOperationRequest.getInventoryQuantity()));
            return ProductOperationSuccess.of(product);

        }catch (ProductDoesNotExists e) {
           return ProductOperationFailure.of(null);        }
    }
    public ProductOperation findById(Long productId){
        String sql = "select id,name,description,baseprice,taxrate,status,inventoryquantity from products where ID = ?";
        Object[] args = {productId};
        RowMapper<Product> rowMapper = (rs, rowNum) -> Product.from(rs.getLong("ID"),
                Name.of(rs.getString("name")), Description.of(rs.getString("description")),
                BasePrice.of(rs.getBigDecimal("baseprice")),TaxRate.of(rs.getBigDecimal("taxrate")),
                ProductStatus.valueOf(rs.getString("status")),InventoryQuantity.of(rs.getInt("inventoryquantity")));
    try {
        Product product =   jdbcTemplate.queryForObject(sql,args, rowMapper);
        return ProductOperationSuccess.of(product);
    }catch (ProductDoesNotExists e){
        return ProductOperationFailure.of(null);
    }

    }
    public List<Product> findAll(){
        String sql = "select id,name,description,baseprice,taxrate,status,inventoryquantity from products";
        RowMapper<Product> rowMapper = (rs, rowNum) -> Product.from(rs.getLong("id"),
                Name.of(rs.getString("name")), Description.of(rs.getString("description")),
                BasePrice.of(rs.getBigDecimal("baseprice")),TaxRate.of(rs.getBigDecimal("taxrate")),
                ProductStatus.valueOf(rs.getString("status")),InventoryQuantity.of(rs.getInt("inventoryquantity")));
            return jdbcTemplate.query(sql, rowMapper);

    }
    public ProductOperation updateOne(Long productId, ProductOperationRequest productOperationRequest){
        String sql = "update  PRODUCTS set name = ?, description = ?,baseprice = ?,taxrate = ? ,status = ?,inventoryquantity = ? where ID = ?";
        KeyHolder keyHolder = new GeneratedKeyHolder();


        PreparedStatementCreator psc = connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, productOperationRequest.getName());
            ps.setString(2, productOperationRequest.getDescription());
            ps.setBigDecimal(3, productOperationRequest.getBasePrice());
            ps.setBigDecimal(4, productOperationRequest.getTaxRate());
            ps.setString(5,productOperationRequest.getProductStatus());
            ps.setInt(6, productOperationRequest.getInventoryQuantity());
            ps.setLong(7 , productId);
            return ps;
        };
        try {
            jdbcTemplate.update(
                    psc,
                    keyHolder
            );
            Product product = Product.from(productId, Name.of(productOperationRequest.getName()),Description.of(productOperationRequest.getDescription()),BasePrice.of(productOperationRequest.getBasePrice())
                    ,TaxRate.of(productOperationRequest.getTaxRate()),ProductStatus.valueOf(productOperationRequest.getProductStatus()), InventoryQuantity.of(productOperationRequest.getInventoryQuantity()));
            return ProductOperationSuccess.of(product);
        }catch (ProductDoesNotExists e){
            ProductDoesNotExists exception = ProductDoesNotExists.of(productId);
            return ProductOperationFailure.of(null);
        }
    }
    public void deleteOne(Long productId) {
        String sql = "delete from  PRODUCTS where ID = ?";
        Object[] args = {productId};

        jdbcTemplate.update(sql, args);
    }

}
