package co.edu.ff.orders.productos.domain;

import co.edu.ff.orders.productos.entity.Product;
import co.edu.ff.orders.productos.exceptions.ProductException;
import lombok.Value;

@Value(staticConstructor = "of")
public class ProductOperationFailure implements ProductOperation {
    ProductException productException;

    @Override
    public Product value() {
        return null;
    }

    @Override
    public String failure() {

        return productException.getMessage();  }

    @Override
    public Boolean isValid() {
        return null;
    }

}
