package co.edu.ff.orders.productos.services;


import co.edu.ff.orders.productos.domain.ProductOperation;
import co.edu.ff.orders.productos.domain.ProductOperationFailure;
import co.edu.ff.orders.productos.domain.ProductOperationSuccess;
import co.edu.ff.orders.productos.entity.Product;
import co.edu.ff.orders.productos.entity.ProductOperationRequest;
import co.edu.ff.orders.productos.exceptions.ProductDoesNotExists;
import co.edu.ff.orders.productos.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class ProductServices {
    ProductRepository productRepository;
    public ProductOperation insertOne(ProductOperationRequest productOperationRequest){
       return productRepository.insertOne(productOperationRequest);
    }
    public ProductOperation findById(Long productId){
        return productRepository.findById(productId);
    }
    public List<Product> findAll(){
        return productRepository.findAll();
    }
    public ProductOperation updateOne(Long productId, ProductOperationRequest productOperationRequest){
        Product product= productRepository.findById(productId).value();
        if (product != null) {
        return productRepository.updateOne(productId,productOperationRequest);
        }
        return productRepository.findById(productId);
    }
    public ProductOperation deleteOne(Long productId){
        Product product= productRepository.findById(productId).value();
        if (product != null) {
            productRepository.deleteOne(productId);
            return ProductOperationSuccess.of(product);
        }
        ProductDoesNotExists exception = ProductDoesNotExists.of(productId);
        return ProductOperationFailure.of(exception);
    }
}
