package co.edu.ff.orders.productos.entity;

import co.edu.ff.orders.productos.precondiciones.Precondiciones;
import co.edu.ff.orders.productos.serialization.LongSerialization;
import lombok.Value;

@Value(staticConstructor = "of")
public class ProductId implements LongSerialization {

    Long productId;

    private ProductId(Long productId){
        Precondiciones.nonNullLong(productId);
        Precondiciones.mayorLong(productId);
        this.productId = productId;
    }

    @Override
    public Long valueOf() {
        return productId;
    }
}
