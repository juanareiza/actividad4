package co.edu.ff.orders.productos.entity;

import co.edu.ff.orders.productos.precondiciones.Precondiciones;
import co.edu.ff.orders.productos.serialization.StringSerialization;
import lombok.Value;

@Value(staticConstructor = "of")
public class Name implements StringSerialization {

    String name;

    private Name(String name){
        Precondiciones.nonNullString(name);
        Precondiciones.maxLengthString(name);
        this.name = name;
    }

    @Override
    public String valueOf() {
        return name;
    }
}
