package co.edu.ff.orders.productos.controllers;


import co.edu.ff.orders.productos.entity.Product;
import co.edu.ff.orders.productos.entity.ProductId;
import co.edu.ff.orders.productos.entity.ProductOperationRequest;
import co.edu.ff.orders.productos.services.ProductServices;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/products")
@AllArgsConstructor
public class ProductController {
    ProductServices productServices;

    @PostMapping("/")
    public ResponseEntity<Product> insertOne(@RequestBody ProductOperationRequest productOperationRequest){
        Product product = productServices.insertOne(productOperationRequest).value();
        if (product != null){
            return ResponseEntity.ok(product);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(product);
        }
    }
    @GetMapping("/{productId}")
    public ResponseEntity<Product> findById(@PathVariable Long productId){
        Product product = productServices.findById(productId).value();
        if (product != null){
            return ResponseEntity.ok(product);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(product);
        }
    }
    @GetMapping("/")
    public ResponseEntity<List<Product>> findAll(){
        List<Product> product = productServices.findAll();
        if (product != null){
            return ResponseEntity.ok(product);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(product);
        }
    }
    @PutMapping("/{productId}")
    public ResponseEntity<Product> update(@PathVariable Long productId,@RequestBody ProductOperationRequest productOperationRequest){
        Product product = productServices.updateOne(productId,productOperationRequest).value();
        if (product != null){
            return ResponseEntity.ok(product);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(product);
        }
    }
    @DeleteMapping("/{productId}")
    public ResponseEntity<Product> deleteById(@PathVariable Long productId){
        Product product = productServices.deleteOne(productId).value();
        if (product != null){
            return ResponseEntity.ok(product);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(product);
        }
    }
}
