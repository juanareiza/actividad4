package co.edu.ff.orders.productos.serialization;

public interface IntegerSerialization {
    Integer valueOf();
}
