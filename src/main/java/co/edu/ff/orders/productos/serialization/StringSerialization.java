package co.edu.ff.orders.productos.serialization;

public interface StringSerialization {
    String valueOf();
}
