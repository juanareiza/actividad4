package co.edu.ff.orders.productos.entity;

import co.edu.ff.orders.productos.precondiciones.Precondiciones;
import co.edu.ff.orders.productos.serialization.BigSerialization;
import lombok.Value;

import java.math.BigDecimal;

@Value(staticConstructor = "of")
public class TaxRate implements BigSerialization {

    BigDecimal taxRate;

    private TaxRate(BigDecimal taxRate){
        Precondiciones.nonNullBig(taxRate);
        Precondiciones.mayorMenorBig(taxRate);
        this.taxRate = taxRate;
    }

    @Override
    public BigDecimal valueOf() {
        return taxRate;
    }
}
