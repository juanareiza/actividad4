package co.edu.ff.orders.productos.entity;

import co.edu.ff.orders.productos.precondiciones.Precondiciones;
import co.edu.ff.orders.productos.serialization.StringSerialization;
import lombok.Value;

@Value(staticConstructor = "of")
public class Description implements StringSerialization {

    String description;

    private Description(String description){
        Precondiciones.nonNullString(description);
        Precondiciones.maxLengthStringDes(description);
        this.description = description;
    }

    @Override
    public String valueOf() {
        return description;
    }
}
