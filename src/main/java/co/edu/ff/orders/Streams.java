package co.edu.ff.orders;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Streams {
    public static void main(String[] args) {
        List<Pizza> pizzaList = Arrays.asList(
                new Pizza("Básica", Size.SMALL, 600),
                new Pizza("Familiar", Size.LARGE, 1800),
                new Pizza("Vegetariana", Size.LARGE, 860),
                new Pizza("Solo queso", Size.MEDIUM, 1000),
                new Pizza("Hawaiana", Size.SMALL, 1200),
                new Pizza("Extra carnes", Size.LARGE, 2100),
                new Pizza("Pollo", Size.SMALL, 900),
                new Pizza("Pollo + tocineta", Size.MEDIUM, 1500),
                new Pizza("Pollo + Jamon", Size.MEDIUM, 1300)

        );

        /*
         * 1. Obtener todas las pizzas de tamaño "MEDIUM"
         */
        List<Pizza> pizza =  pizzaList.stream().filter(i -> i.getSize() == Size.MEDIUM).collect(Collectors.toList());
        System.out.println("Pizzas con tamaño medium");
        System.out.println(pizza);

        /*
         * 2. Obtener todas las pizzas que las calorias esten entre 700 y 1500
         */
         pizza =  pizzaList.stream().filter(i -> i.getCalories() >= 700 && i.getCalories() <= 1500).collect(Collectors.toList());
        System.out.println("Pizzas entre 700 y 1500 calorias");
        System.out.println(pizza);
        /*
         * 3. Obtener las 3 pizzas con más calorias
         */

        pizza =  pizzaList.stream().sorted(Comparator.comparingInt(Pizza::getCalories).reversed()).collect(Collectors.toList());
        System.out.println("Pizzas con mas calorias");
        System.out.println(pizza.get(0));
        System.out.println(pizza.get(1));
        System.out.println(pizza.get(2));



        /*
         * 4. Obtener las 2 pizzas con menos calorias
         */
        pizza =  pizzaList.stream().sorted(Comparator.comparingInt(Pizza::getCalories)).collect(Collectors.toList());
        System.out.println("Pizas con menos calorias");
        System.out.println(pizza.get(0));
        System.out.println(pizza.get(1));
        /*
         * 5. Del numeral 2 obtener las 2 pizzas con mas calorias
         */
        pizza =  pizzaList.stream().filter(i -> i.getCalories() >= 700 && i.getCalories() <= 1500).collect(Collectors.toList());
        pizza =  pizza.stream().sorted(Comparator.comparingInt(Pizza::getCalories).reversed()).collect(Collectors.toList());
        System.out.println("Mas calorias Numeral 2");
        System.out.println(pizza.get(0));
        System.out.println(pizza.get(1));

        /*
         * 5. Agrupar las pizzas por tamaño
         */
        pizza = pizzaList.stream().sorted(Comparator.comparing(Pizza::getSize)).collect(Collectors.toList());
        System.out.println("Ordenado por tamaño: "+ pizza);
        /*
         * 6. Agrupar las pizzas por los siguientes grupos:
         * de 0 a 1000 calorias
         * de 1001 a 2000 calorias
         * de 2001 a 3000 calorias
         */
        List pizza1 =  pizzaList.stream().filter(i -> i.getCalories() >= 0 && i.getCalories() <= 1000).collect(Collectors.toList());
        List pizza2 =  pizzaList.stream().filter(i -> i.getCalories() >= 1001 && i.getCalories() <= 2000).collect(Collectors.toList());
        List pizza3 =  pizzaList.stream().filter(i -> i.getCalories() >= 2001 && i.getCalories() <= 3000).collect(Collectors.toList());
        System.out.println("Pizzas agrupadas por calorias");
        System.out.println(pizza1);
        System.out.println(pizza2);
        System.out.println(pizza3);


    }

    public enum Size {
        SMALL,
        MEDIUM,
        LARGE
    }

    public static class Pizza {
        private final String name;
        private final Size size;
        private final Integer calories;

        public Pizza(String name, Size size, Integer calories) {
            this.name = name;
            this.size = size;
            this.calories = calories;
        }

        public Size getSize() {
            return size;
        }

        public String getName() {
            return name;
        }

        public Integer getCalories() {
            return calories;
        }

        @Override
        public String toString() {
            return String.format("Pizza{%s, %s, %s}", name, size, calories);
        }
    }
}
