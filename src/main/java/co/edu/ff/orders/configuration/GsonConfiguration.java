package co.edu.ff.orders.configuration;


import co.edu.ff.orders.productos.entity.*;
import co.edu.ff.orders.productos.serialization.BigAdapter;
import co.edu.ff.orders.productos.serialization.IntegerAdapter;
import co.edu.ff.orders.productos.serialization.LongAdapter;
import co.edu.ff.orders.productos.serialization.StringAdapter;
import com.google.gson.*;
import io.vavr.control.Try;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;

@Configuration
public class GsonConfiguration {

    @Bean
    public Gson gson() {

        return new GsonBuilder()
                .registerTypeAdapter(BasePrice.class, new BigAdapter<>(BasePrice::of))
                .registerTypeAdapter(TaxRate.class, new BigAdapter<>(TaxRate::of))
                .registerTypeAdapter(ProductId.class, new LongAdapter<>(ProductId::of))
                .registerTypeAdapter(Description.class, new StringAdapter<>(Description::of))
                .registerTypeAdapter(Name.class, new StringAdapter<>(Name::of))
                .registerTypeAdapter(InventoryQuantity.class, new IntegerAdapter<>(InventoryQuantity::of))
                .create();
    }
}
