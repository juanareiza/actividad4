package co.edu.ff.orders.productos.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ProductIdTest {
    @Test
    @DisplayName("No debería crear id para casos inválidos")
    void isShouldNotPass() {
        Long l1 = null;
        Long l2 = 2l;
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> ProductId.of(l1)),
                () -> assertThrows(IllegalArgumentException.class, () -> ProductId.of(l2))
        );
    }

    @TestFactory
    @DisplayName("Debería crear id de inventarios validas")
    Stream<DynamicTest> isShouldPass() {
        return Stream.of(
                1,
                0,
                1
        )
                .map(productId -> {
                    String testName = String.format("debería ser valido para id: %s", productId);
                    Executable executable = () -> {

                        ThrowingSupplier<ProductId> productIdThrowingSupplier = () -> ProductId.of(Long.valueOf(productId));

                        assertAll(
                                () -> assertDoesNotThrow(productIdThrowingSupplier),
                                () -> assertNotNull(productIdThrowingSupplier.get())
                        );
                    };
                    return DynamicTest.dynamicTest(testName, executable);
                });
    }
}