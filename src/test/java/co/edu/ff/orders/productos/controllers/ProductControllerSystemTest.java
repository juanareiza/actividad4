package co.edu.ff.orders.productos.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ProductControllerSystemTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void createAndFindProduct() throws Exception {
        String name = "Juan";
        String description = "Hola";
        BigDecimal basePrice = BigDecimal.valueOf(23);
        BigDecimal taxRate = BigDecimal.ONE;
        String productStatus = "BORRADOR";
        Integer invetoryQuantity  = 1;
        String json = String.format("{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": \"%s\", \"inventoryQuantity\": %s}", name, description,basePrice,taxRate,productStatus,invetoryQuantity);
        String createdJson = String.format("{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": %s, \"inventoryQuantity\": %s,\"id\":1}", name, description,basePrice,taxRate,productStatus,invetoryQuantity);
        String findJson = String.format("{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": \"%s\", \"inventoryQuantity\": %s,\"id\":1}",name, description,basePrice.toString(),taxRate.toString(),productStatus,invetoryQuantity.toString());
        mockMvc.perform(
                post("/api/v1/products/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        )
                .andExpect(status().isOk())
                .andExpect(content().json(createdJson));
        mockMvc.perform(get("/api/v1/products/1"))

                .andExpect(status().isOk())
                .andExpect(content().json(findJson));
    }
    @Test
    void createAndFindProducts() throws Exception {
        String name = "Juan";
        String description = "Hola";
        BigDecimal basePrice = BigDecimal.valueOf(23);
        BigDecimal taxRate = BigDecimal.ONE;
        String productStatus = "BORRADOR";
        Integer invetoryQuantity  = 1;
        String json = String.format("{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": \"%s\", \"inventoryQuantity\": %s}", name, description,basePrice,taxRate,productStatus,invetoryQuantity);
        String createdJson = String.format("{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": %s, \"inventoryQuantity\": %s,\"id\":1}", name, description,basePrice,taxRate,productStatus,invetoryQuantity);
        String findJsons = String.format("[{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": \"%s\", \"inventoryQuantity\": %s,\"id\":1},{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": \"%s\", \"inventoryQuantity\": %s,\"id\":2}]",name, description,basePrice.toString(),taxRate.toString(),productStatus,invetoryQuantity.toString(),name, description,basePrice.toString(),taxRate.toString(),productStatus,invetoryQuantity.toString());
        mockMvc.perform(
                post("/api/v1/products/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        )
                .andExpect(status().isOk())
                .andExpect(content().json(createdJson));
        mockMvc.perform(
                post("/api/v1/products/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        )
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/v1/products/"))
                .andExpect(status().isOk())
                .andExpect(content().json(findJsons));
    }
    @Test
    void createDeleteFindProducts() throws Exception {
        String name = "Juan";
        String description = "Hola";
        BigDecimal basePrice = BigDecimal.valueOf(23);
        BigDecimal taxRate = BigDecimal.ONE;
        String productStatus = "BORRADOR";
        Integer invetoryQuantity  = 1;
        String json = String.format("{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": \"%s\", \"inventoryQuantity\": %s}", name, description,basePrice,taxRate,productStatus,invetoryQuantity);
        String createdJson = String.format("{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": %s, \"inventoryQuantity\": %s,\"id\":1}", name, description,basePrice,taxRate,productStatus,invetoryQuantity);
        String findJsons = String.format("[{\"name\": \"%s\", \"description\": \"%s\", \"basePrice\": %s, \"taxRate\": %s, \"productStatus\": \"%s\", \"inventoryQuantity\": %s,\"id\":2}]",name, description,basePrice.toString(),taxRate.toString(),productStatus,invetoryQuantity.toString(),name, description,basePrice.toString(),taxRate.toString(),productStatus,invetoryQuantity.toString());
        mockMvc.perform(
                post("/api/v1/products/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        )
                .andExpect(status().isOk())
                .andExpect(content().json(createdJson));
        mockMvc.perform(
                post("/api/v1/products/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
        )
                .andExpect(status().isOk());
        mockMvc.perform(delete("/api/v1/products/1")).andExpect(status().isOk());
        mockMvc.perform(get("/api/v1/products/"))
                .andExpect(status().isOk())
                .andExpect(content().json(findJsons));

    }
}