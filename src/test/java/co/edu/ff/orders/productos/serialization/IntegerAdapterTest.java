package co.edu.ff.orders.productos.serialization;

import co.edu.ff.orders.productos.entity.InventoryQuantity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IntegerAdapterTest {
    static Gson gson;

    @BeforeAll
    static void setUp() {
        gson = new GsonBuilder()
                .registerTypeAdapter(InventoryQuantity.class, new IntegerAdapter<>(InventoryQuantity::of))
                .create();
    }

    @Test
    void deserialize() {
    }

    @Test
    void serialize() {

        Integer integer = 1;
        InventoryQuantity inventoryQuantity = InventoryQuantity.of(integer);


        String actual = String.format("\"%s\"",gson.toJson(inventoryQuantity));

        String expected = String.format("\"%s\"", inventoryQuantity.getInventoryQuantity());
        assertEquals(actual, expected);
    }

}