package co.edu.ff.orders.productos.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TaxRateTest {

    @Test
    @DisplayName("No debería crear taxRate para casos inválidos")
    void isShouldNotPass() {
        BigDecimal t1 = null;
        BigDecimal t2 = BigDecimal.valueOf(0);
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> TaxRate.of(t1)),
                () -> assertThrows(IllegalArgumentException.class, () -> TaxRate.of(t2))
        );
    }

    @TestFactory
    @DisplayName("Debería crear taxRate validas")
    Stream<DynamicTest> isShouldPass() {
        return Stream.of(
                1,
                2,
                3
        )
                .map(taxRate -> {
                    String testName = String.format("debería ser valido para el taxRate: %s", taxRate);
                    Executable executable = () -> {

                        ThrowingSupplier<TaxRate> taxRateThrowingSupplier = () -> TaxRate.of(BigDecimal.valueOf(taxRate));
                        assertAll(
                                () -> assertDoesNotThrow(taxRateThrowingSupplier),
                                () -> assertNotNull(taxRateThrowingSupplier.get())
                        );
                    };
                    return DynamicTest.dynamicTest(testName,  executable);
                });
    }


}