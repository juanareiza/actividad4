package co.edu.ff.orders.productos.serialization;

import co.edu.ff.orders.productos.entity.Name;
import co.edu.ff.orders.productos.entity.TaxRate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class BigAdapterTest {
    static Gson gson;

    @BeforeAll
    static void setUp() {
        gson = new GsonBuilder()
                .registerTypeAdapter(TaxRate.class, new BigAdapter<>(TaxRate::of))
                .create();
    }

    @Test
    void deserialize() {
    }

    @Test
    void serialize() {

        BigDecimal tax = BigDecimal.ONE;
        TaxRate taxRate = TaxRate.of(tax);


        String actual = String.format("\"%s\"",gson.toJson(taxRate));

        String expected = String.format("\"%s\"", taxRate.getTaxRate());
        assertEquals(actual, expected);
    }

}