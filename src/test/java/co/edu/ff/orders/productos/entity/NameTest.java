package co.edu.ff.orders.productos.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class NameTest {
    @Test
    @DisplayName("No debería crear nombre para casos inválidos")
    void isShouldNotPass() {
        String n1 = null;
        String n2 = "Probando el tamañano 100 valido para name pero que falle, Probando el tamañano 100 valido para name pero que falle";
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> Name.of(n1)),
                () -> assertThrows(IllegalArgumentException.class, () -> Name.of(n2))
        );
    }

    @TestFactory
    @DisplayName("Debería crear nombre validas")
    Stream<DynamicTest> isShouldPass() {
        return Stream.of(
                "Hola",
                "Si",
                "No"
        )
                .map(name -> {
                    String testName = String.format("debería ser valido para nombre: %s", name);
                    Executable executable = () -> {

                        ThrowingSupplier<Name> nameThrowingSupplier = () -> Name.of(name);

                        assertAll(
                                () -> assertDoesNotThrow(nameThrowingSupplier),
                                () -> assertNotNull(nameThrowingSupplier.get())
                        );
                    };
                    return DynamicTest.dynamicTest(testName, executable);
                });
    }

}