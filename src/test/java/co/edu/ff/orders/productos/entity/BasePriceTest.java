package co.edu.ff.orders.productos.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class BasePriceTest {
    @Test
    @DisplayName("No debería crear BasePrice para casos inválidos")
    void isShouldNotPass() {
        BigDecimal b1 = null;
        BigDecimal b2 = BigDecimal.valueOf(0);
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> BasePrice.of(b1)),
                () -> assertThrows(IllegalArgumentException.class, () -> BasePrice.of(b2))
        );
    }

    @TestFactory
    @DisplayName("Debería crear taxRate validas")
    Stream<DynamicTest> isShouldPass() {
        return Stream.of(
                1,
                2,
                3
        )
                .map(basePrice -> {
                    String testName = String.format("debería ser valido para el baseprice: %s", basePrice);
                    Executable executable = () -> {

                        ThrowingSupplier<BasePrice> basePriceThrowingSupplier = () -> BasePrice.of(BigDecimal.valueOf(basePrice));
                        assertAll(
                                () -> assertDoesNotThrow(basePriceThrowingSupplier),
                                () -> assertNotNull(basePriceThrowingSupplier.get())
                        );
                    };
                    return DynamicTest.dynamicTest(testName,  executable);
                });
    }


}