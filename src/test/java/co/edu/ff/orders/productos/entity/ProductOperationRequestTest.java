package co.edu.ff.orders.productos.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class ProductOperationRequestTest {
    @Test
    @DisplayName("No debería crear producto request para casos inválidos")
    void isShouldNotPass() {
        String nombre = null;
        String descip = null;
        BigDecimal base = null;
        BigDecimal tax = null;
        String status = null;
        Integer inventory = null;
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> ProductOperationRequest.of(nombre,descip,base,tax,status,inventory))
        );
    }
    @Test
    @DisplayName(" debería crear producto para casos válidos")
    void isShouldPass() {
        String nombre = "Hola";
        String descip = "Hola";
        BigDecimal base = BigDecimal.ONE;
        BigDecimal tax = BigDecimal.ONE;
        String status = "BORRADOR";
        Integer inventory = 1;
        assertAll(
                () -> assertDoesNotThrow(() -> ProductOperationRequest.of(nombre,descip,base,tax,status,inventory))
        );
    }

}