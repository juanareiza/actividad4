package co.edu.ff.orders.productos.serialization;

import co.edu.ff.orders.productos.entity.Name;
import co.edu.ff.orders.productos.serialization.StringAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringValueAdapterTest {
    static Gson gson;

    @BeforeAll
    static void setUp() {
        gson = new GsonBuilder()
                .registerTypeAdapter(Name.class, new StringAdapter<>(Name::of))
                .create();
    }

    @Test
    void deserialize() {
    }

    @Test
    void serialize() {

        String name = "name";
        Name nom = Name.of(name);


        String actual = gson.toJson(nom);

        String expected = String.format("\"%s\"", nom.getName());
        assertEquals(actual, expected);
    }

}