package co.edu.ff.orders.productos.controllers;

import co.edu.ff.orders.productos.domain.ProductOperationFailure;
import co.edu.ff.orders.productos.domain.ProductOperationSuccess;
import co.edu.ff.orders.productos.entity.*;
import co.edu.ff.orders.productos.services.ProductServices;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Gson gson;

    @MockBean
    ProductServices services;


    @Test
    void findProductEmpty() throws Exception {
        // organizar....
        when(services.findById(anyLong()))
                .thenReturn(ProductOperationFailure.of(null));

        // act
        // assert
        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.get("/api/v1/products/1");
        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void findProduct() throws Exception {
        // organizar....
        Product product = Product.from(
                1L,
                Name.of("Juan"),
                Description.of("Hola"),
                BasePrice.of(BigDecimal.ONE),
                TaxRate.of(BigDecimal.ONE),
                ProductStatus.valueOf("BORRADOR"),
                InventoryQuantity.of(1)
        );
        String productJson = this.gson.toJson(product);
        when(services.findById(anyLong()))
                .thenReturn(ProductOperationSuccess.of(product));
        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.get("/api/v1/products/1");
        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(productJson));
    }
    @Test
    void findProductsEmpty() throws Exception {
        // organizar....
        List<Product> products = null;
        when(services.findAll())
                .thenReturn(products);

        // act
        // assert
        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.get("/api/v1/products/");
        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void findProducts() throws Exception {
        // organizar....
        List<Product> products = new ArrayList<>();
        Product product = Product.from(
                1L,
                Name.of("Juan"),
                Description.of("Hola"),
                BasePrice.of(BigDecimal.ONE),
                TaxRate.of(BigDecimal.ONE),
                ProductStatus.valueOf("BORRADOR"),
                InventoryQuantity.of(1)
        );
        products.add(product);
        String productJson = this.gson.toJson(products);
        when(services.findAll())
                .thenReturn(products);
        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.get("/api/v1/products/");
        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(productJson));
    }
    @Test
    void insertProductFail() throws Exception {
        // organizar....
        ProductOperationRequest product = ProductOperationRequest.of(
                "Juan",
                "Hola",
                BigDecimal.ONE,
                BigDecimal.ONE,
                "BORRADOR",
                1
        );
        when(services.insertOne(product))
                .thenReturn(null);

        // act
        // assert
        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.post("/api/v1/products/");
        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void insertProducts() throws Exception {
        // organizar....
        ProductOperationRequest productRequest = ProductOperationRequest.of(
                "Juan",
                "Hola",
                BigDecimal.valueOf(123),
                BigDecimal.ONE,
                "BORRADOR",
                123
        );
        Product product = Product.from(
                1L,
                Name.of("Juan"),
                Description.of("Hola"),
                BasePrice.of(BigDecimal.valueOf(123)),
                TaxRate.of(BigDecimal.ONE),
                ProductStatus.valueOf("BORRADOR"),
                InventoryQuantity.of(123)
        );
        String productJson = this.gson.toJson(productRequest);
        when(services.insertOne(any()))
                .thenReturn(ProductOperationSuccess.of(product));
        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.post("/api/v1/products/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(productJson);

        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(productJson));
    }
    @Test
    void updateProductFail() throws Exception {
        // organizar....
        ProductOperationRequest product = ProductOperationRequest.of(
                "Juan",
                "Hola",
                BigDecimal.ONE,
                BigDecimal.ONE,
                "BORRADOR",
                1
        );
        when(services.updateOne(1L,product))
                .thenReturn(null);

        // act
        // assert
        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.put("/api/v1/products/1");
        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void updateProducts() throws Exception {
        // organizar....
        ProductOperationRequest productRequest = ProductOperationRequest.of(
                "Juan",
                "Hola",
                BigDecimal.ONE,
                BigDecimal.ONE,
                "BORRADOR",
                1
        );
        Product product = Product.from(
                1L,
                Name.of("Juan"),
                Description.of("Hola"),
                BasePrice.of(BigDecimal.ONE),
                TaxRate.of(BigDecimal.ONE),
                ProductStatus.valueOf("BORRADOR"),
                InventoryQuantity.of(1)
        );
        String productJson = this.gson.toJson(productRequest);
        when(services.updateOne(anyLong(),any()))
                .thenReturn(ProductOperationSuccess.of(product));

        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.put("/api/v1/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .param("id","1")
                .content(productJson);;
        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(productJson));
    }
    @Test
    void deleteProductFail() throws Exception {
        // organizar....
        when(services.deleteOne(1L))
                .thenReturn(null);

        // act
        // assert
        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.delete("/api/v1/products/1");
        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void deleteProducts() throws Exception {
        // organizar....
        Product product = Product.from(
                1L,
                Name.of("Juan"),
                Description.of("Hola"),
                BasePrice.of(BigDecimal.ONE),
                TaxRate.of(BigDecimal.ONE),
                ProductStatus.valueOf("BORRADOR"),
                InventoryQuantity.of(1)
        );
        String productJson = this.gson.toJson(product);
        when(services.deleteOne(1L))
                .thenReturn(ProductOperationSuccess.of(product));
        MockHttpServletRequestBuilder servletRequestBuilder = MockMvcRequestBuilders.delete("/api/v1/products/1")
                .param("id","1");
        this.mockMvc.perform(servletRequestBuilder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(productJson));
    }
}