package co.edu.ff.orders.productos.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class DescriptionTest {
    @Test
    @DisplayName("No debería crear descripcion para casos inválidos")
    void isShouldNotPass() {
        String d1 = null;
        String d2 = "Probando el tamañan de la descripcion sea menos a 280 palabras, Probando el tamañan de la descripcion sea menos a 280 palabras, Probando el tamañan de la descripcion sea menos a 280 palabras, Probando el tamañan de la descripcion sea menos a 280 palabras, Probando el tamañan de la descripcion sea menos a 280 palabras, Probando el tamañan de la descripcion sea menos a 280 palabras, Probando el tamañan de la descripcion sea menos a 280 palabras ";
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> Description.of(d1)),
                () -> assertThrows(IllegalArgumentException.class, () -> Description.of(d2))
        );
    }

    @TestFactory
    @DisplayName("Debería crear descripciones validas")
    Stream<DynamicTest> isShouldPass() {
        return Stream.of(
                "Hola",
                "Si",
                "No"
        )
                .map(descripcion -> {
                    String testName = String.format("debería ser valido para descripcion: %s", descripcion);
                    Executable executable = () -> {

                        ThrowingSupplier<Description> descriptionThrowingSupplier = () -> Description.of(descripcion);

                        assertAll(
                                () -> assertDoesNotThrow(descriptionThrowingSupplier),
                                () -> assertNotNull(descriptionThrowingSupplier.get())
                        );
                    };
                    return DynamicTest.dynamicTest(testName, executable);
                });
    }
}