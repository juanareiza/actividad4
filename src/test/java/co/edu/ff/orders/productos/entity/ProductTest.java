package co.edu.ff.orders.productos.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {






    @Test
    @DisplayName("No debería crear producto para casos inválidos")
    void isShouldNotPass() {
        Long n1 = null;
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> Product.from(n1,Name.of("Hola"),Description.of("hola"),BasePrice.of(BigDecimal.ONE),TaxRate.of(BigDecimal.ONE),ProductStatus.valueOf("Borrador"),InventoryQuantity.of(1)))
        );
    }
    @Test
    @DisplayName(" debería crear producto para casos válidos")
    void isShouldPass() {
        Long n1 = 1L;
        assertAll(
                () -> assertDoesNotThrow(() -> Product.from(n1,Name.of("Hola"),Description.of("hola"),BasePrice.of(BigDecimal.ONE),TaxRate.of(BigDecimal.ONE),ProductStatus.valueOf("BORRADOR"),InventoryQuantity.of(1)))
        );
    }

}