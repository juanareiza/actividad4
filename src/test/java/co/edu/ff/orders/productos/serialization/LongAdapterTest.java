package co.edu.ff.orders.productos.serialization;

import co.edu.ff.orders.productos.entity.InventoryQuantity;
import co.edu.ff.orders.productos.entity.ProductId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongAdapterTest {
    static Gson gson;

    @BeforeAll
    static void setUp() {
        gson = new GsonBuilder()
                .registerTypeAdapter(ProductId.class, new LongAdapter<>(ProductId::of))
                .create();
    }

    @Test
    void deserialize() {
    }

    @Test
    void serialize() {

        Long id = 1l;
        ProductId productId = ProductId.of(id);


        String actual = String.format("\"%s\"",gson.toJson(productId));

        String expected = String.format("\"%s\"", productId.getProductId());
        assertEquals(actual, expected);
    }

}