package co.edu.ff.orders.productos.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class InventoryQuantityTest {
    @Test
    @DisplayName("No debería crear cantidad de inventario para casos inválidos")
    void isShouldNotPass() {
        Integer i1 = null;
        Integer i2 = -1;
                assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> InventoryQuantity.of(i1)),
                () -> assertThrows(IllegalArgumentException.class, () -> InventoryQuantity.of(i2))
        );
    }

    @TestFactory
    @DisplayName("Debería crear cantidad de inventarios validas")
    Stream<DynamicTest> isShouldPass() {
        return Stream.of(
                1,
                10,
                11
        )
                .map(inventory -> {
                    String testName = String.format("debería ser valido para cantidad de inventario: %s", inventory);
                    Executable executable = () -> {

                        ThrowingSupplier<InventoryQuantity> inventoryQuantityThrowingSupplier = () -> InventoryQuantity.of(inventory);

                        assertAll(
                                () -> assertDoesNotThrow(inventoryQuantityThrowingSupplier),
                                () -> assertNotNull(inventoryQuantityThrowingSupplier.get())
                        );
                    };
                    return DynamicTest.dynamicTest(testName, executable);
                });
    }
}